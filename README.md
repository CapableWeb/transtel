## transtel

Translates any messages (freeform text or forwarded messages) into English and
sends it back to you. Translation via DeepL (API key required)

MIT License.
