use std::env;

use deepl_api::{DeepL, TranslatableTextList};
use futures::StreamExt;
use rand::prelude::*;
use std::collections::HashMap;
use telegram_bot::*;

async fn translate(deepl: &DeepL, text_to_translate: String) -> String {
    let texts = TranslatableTextList {
        source_language: None,
        target_language: "EN-US".to_string(),
        texts: vec![text_to_translate],
    };

    let translated = deepl.translate(None, texts);
    let translated_text = match translated {
        Ok(translated) => translated[0].text.clone(),
        Err(err) => {
            println!("{:#?}", err);
            let err_str = err.0.to_string();
            format!("{err_str}\n\nI Failed to translate text, the text might have been too long, or we ran out of funds for now... Please try again at another time.")
        }
    };
    translated_text
}

async fn usage_text(deepl: &DeepL) -> String {
    let usage_stats = deepl.usage_information().unwrap();
    let character_limit = usage_stats.character_limit;
    let character_count = usage_stats.character_count;
    let characters_left = character_limit - character_count;
    let usage_text = format!("Transtel currently have {} characters left on our translation plan with DeepL, out of a total of {} per month.", characters_left, character_limit);
    usage_text
}

async fn donate_text(deepl: &DeepL) -> String {
    let donate_message = format!("{}\nCurrent monthly costs for running Transtel is around ~80 EUR per month. If you can, please support us financially.\n\nVia Monero: 86RinhjWWHdce7pzu5BFmMRKxYquCuXrwAdETcbiJckoF7zm6RSjryiKUJydPv21iFNWJ8VoCi3FWgQxqcPGqPMgBJm8ZhP\nVia Ethereum: 0x62890380D18926520a689454D4BDF165f96F6623\nVia Polygon: 0x62890380D18926520a689454D4BDF165f96F6623.\n\nAs we get closer to reaching the current limit, we'll display these messages more often, so to make the service better for everyone, please donate!\n\nIf you have any questions or suggestions, please join our group chat here: https://t.me/+TFQXJe8qXOY4MDFi\n\nThank you for your support!", usage_text(deepl).await);
    donate_message
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let token = env::var("TELEGRAM_BOT_TOKEN").expect("TELEGRAM_BOT_TOKEN not set");
    let deepl = DeepL::new(env::var("DEEPL_API_KEY").expect("DEEPLY_API_KEY not set"));
    let api = Api::new(token);
    let mut rng = thread_rng();
    let mut translation_cache: HashMap<String, String> = HashMap::new();

    // Fetch new updates via long poll method
    let mut stream = api.stream();
    while let Some(update) = stream.next().await {
        // If the received update contains a new message...
        let update = update?;

        if let UpdateKind::Message(message) = update.kind {
            let data = message.text();
            if let Some(data) = data {
                if data == "/start" {
                    api.send(message.text_reply(
                        "Just forward/send any message to me and I'll reply with a translation!",
                    ))
                    .await
                    .unwrap();
                } else {
                    println!("<{}>: {:#?}", &message.from.first_name, data);

                    let rand_num: u16 = rng.gen_range(0..5);
                    let donate_message: bool = rand_num == 0;

                    println!("----");

                    if translation_cache.contains_key(&data) {
                        let text = translation_cache.get(&data).unwrap();
                        println!("Using cached translation: {:#?}", text);
                        let res = api.send(message.text_reply(text)).await.unwrap();
                        if donate_message {
                            api.send(res.text_reply(donate_text(&deepl).await))
                                .await
                                .unwrap();
                            println!("Sent Donate message to {}", &message.from.first_name);
                        }
                    } else {
                        let text = translate(&deepl, data.to_string()).await;

                        translation_cache.insert(data, text.clone());

                        println!("Translation: {:#?}", text);

                        let res = api.send(message.text_reply(text)).await;

                        match res {
                            Ok(res) => {
                                if donate_message {
                                    api.send(res.text_reply(donate_text(&deepl).await))
                                        .await
                                        .unwrap();
                                    println!("Sent Donate message to {}", &message.from.first_name);
                                }
                            }
                            Err(err) => {
                                println!("{:#?}", err);
                            }
                        }
                    }
                }
            }
        }
        println!("###########################");
    }
    Ok(())
}
